fun main() {

    printAll("One", "Two", "Three", "Four")
    printAllWithPrefix("One", "Two", "Three", "Four")
    printAllWithPrefix("One", "Two", "Three", "Four", prefix = "Log ")
    printAllAlternative("One", "Two", "Three", "Four")

}

fun printAll(vararg messages: String) {
    for (message in messages) println(message)
}

fun printAllWithPrefix(vararg messages: String, prefix: String = "Info ") {
    for (message in messages) println(prefix + message)
}

fun printAllAlternative(vararg messages: String) {
    printAll(*messages)
}