fun main() {

    val a = "Initial value"
    println(a)

    var b = 1
    val c = 3

    println(b)
    println(c)

    val d: Int
    d = if (b == c) 1 else 2

    println(d)

}