/**
 * Estudos de funções em Kotlin (versão 1.3)
 *
 * Funções em Kotlin cumprem os mesmo papéis de um método em Java, porém em Kotlin, funções
 * são "first class citizens", assim como em JavaScript, podendo ser passadas como parâmetros, por exemplo.
 * Java também permite o mesmo com Reflections, e agora com Streams, porém de maneira muito mais verbosa.
 */

/**
 * Exemplo de função simples.
 *
 * Funções em Kotlin devem ser identificadas com o sufixo 'fun', seguido do nome da função. É recomendável o uso de
 * camelCase por convenção, nos nomes das funções.
 *
 * A passagem de parametros e por parenteses. Uma mudança comparado ao Java é que a tipagem da variável é realizada
 * após seu nome, como JavaScript. Kotlin por ser interoperável com Java, permite que os tipos das duas linguagens
 * sejam usados.
 *
 * A lógica da função em questão não tem retorno, o que precisaria ser especificado em java com o tipo void. Em Kotlin,
 * funções sem retorno retornam por padrão o tipo Unit. Mas funções sem retorno podem não especificar seu retorno, que
 * é o caso abaixo.
 *
 * A lógica da função é colocada entre colchetes.
 *
 * Dentro da lógica dessa função também existe outra mudança, muitos métodos que o Java encapsula em diversas classes
 * estão disponíveis por padrão na linguagem, se a necessidade de importação ou instanciação. System.out.println é
 * somente println em Kotlin.
 */
fun printMessage(message: String) {
    println(message)
}

/**
 * Função usando named parameters.
 *
 * Em Kotlin, podemos passar os parâmetros como em Java, obedecendo a ordem definida pela função/método ou usando
 * os named parameters. É possível dizer qual é o parâmetro especifico que esta sendo passado, em qualquer ordem.
 * Isso evita a chamada de funções onde mais de um parâmetro pode ser nulo, o que faz a chamada ficar aparentemente
 * furada. Exemplo(Java): myClass.myMethod(null, null, 1). Em Kotlin seria possível fazer:
 * myClass.myFunction(thirdParameter = 1)
 *
 * Além do named parameter, Kotlin também permite valores Default de um parâmetro caso ele não seja passado durante
 * sua chamada. No caso abaixo, caso o segundo parametro for nulo, ou não for passado, essa variavel receberá o valor
 * de "Info"
 */
fun printMessageWithPrefix(message:String, prefix: String = "Info") {
    println("[$prefix] $message")
}

fun sum(x: Int, y: Int): Int {
    return x + y
}

fun multiply (x: Int, y: Int) = x * y

fun main() {
    printMessage("Hello")
    printMessageWithPrefix("Hello", "Log")
    printMessageWithPrefix("Hello")
    printMessageWithPrefix(prefix = "Log", message = "Hello")
    println(sum(3, 2))
    println(multiply(2, 3))
}