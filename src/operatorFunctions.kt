fun main() {

    operator fun Int.times(string: String) = string.repeat(this)
    println (2 * "Bye")

    operator fun String.get(range: IntRange) = substring(range)
    val string = "Always forgive your enemies; nothing annoys them so much."
    print(string[0..14])

}