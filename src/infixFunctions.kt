fun main() {

    infix fun Int.times(string: String) = string.repeat(this)
    println(2 times "Bye")

    val pair = "Ferrari" to "Katrina"
    println(pair)

    infix fun String.onto(other: String) = Pair(this, other)
    val newPair = "McLaren" onto "Lucas"
    println(newPair)

    val sophia = Person("Sophia")
    val claudia = Person("Claudia")
    val ada = Person("Ada")

    sophia likes claudia
    sophia likes ada
    ada likes claudia
    claudia likes ada

    sophia.whoDoYouLike()
    claudia.whoDoYouLike()
    ada.whoDoYouLike()

}

class Person(private val name: String) {

    private val likedPeople = mutableListOf<Person>()

    infix fun likes(other: Person) {
        likedPeople.add(other)
    }

    fun whoDoYouLike() {

        println("$name, who do you like?")

        var message = "I like "
        var notFirst = false

        for (people in likedPeople) {
            if (notFirst) message += ", "
            message += people.name
            notFirst = true
        }

        println(message)

    }

}